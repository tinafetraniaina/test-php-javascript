<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\OrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(AuthController::class)->prefix('auth')->group(function () {
    Route::post('/signin', 'signin');
    Route::post('/signup', 'signup');
});

// Route::group(['middleware' => 'auth:sanctum'], function() {
//     Route::get('posts', [OrderController::class, 'post']);
//     Route::get('posts/{id}', [ControllerExample::class, 'singlePost']);
//     Route::post('orders', [ControllerExample::class, 'createOrder']);
//     Route::put('posts/{id}', [ControllerExample::class, 'updatePost']);
//     Route::delete('posts/{id}', [ControllerExample::class, 'deletePost']);
//     Route::post('users/writer', [ControllerExample::class, 'createWriter']);
//     Route::post('users/subscriber', [ControllerExample::class, 'createSubscriber']);
//     Route::delete('users/{id}', [ControllerExample::class, 'deleteUser']);
// });

Route::middleware('auth:sanctum')->group( function () {
    Route::resource('orders', OrderController::class);
});
