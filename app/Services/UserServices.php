<?php

namespace App\Services;

use App\Exceptions\BlogNotFoundException;
use App\Interfaces\UserRepositoryInterface;

class UserServices
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
    * @param array $data
    * @return mixed
    */
    public function createUser($data)
    {
        $result = $this->userRepository->create($data);
        return $result;
    }
}
