<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Interfaces\OrderRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends BaseController
{
    private $orderRepository;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
    ) {
        $this->orderRepository = $orderRepository;
    }
    public function index()
    {
        $orders = Order::all();
        return $this->sendResponse(OrderResource::collection($orders), 'Orders fetched.');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'total_price' => 'required|integer',
            'products' => 'required'
        ]);
        if($validator->fails()){
            return $this->sendError(400, $validator->errors());
        }

        $data = $validator->validated();
        $dataOrder = $data;
        unset($dataOrder['products']);
        $order = $this->orderRepository->add(array_merge(
            $dataOrder,
            ['user_id' => auth('sanctum')->user()->id]
        ));

        $oderItems = $this->orderRepository->addItems($order->id, $data['products']);
        return $this->sendResponse($order, 'Order created.', 201);
    }
}
