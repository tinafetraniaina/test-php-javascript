<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Services\UserServices;
use Illuminate\Support\Facades\Hash;

class AuthController extends BaseController
{
    public function signin(LoginRequest $request)
    {
        try {
            $user = User::where('email', $request->email)->first();

            if (Hash::check($request->password, $user->password)) {
                if ($user->role === 1) {
                    $results = $this->createNewToken($user->createToken('auth_token', ['user'])->plainTextToken);
                } else {
                    $results = $this->createNewToken($user->createToken('auth_token', ['admin'])->plainTextToken);
                }
                return $this->sendResponse($results, 'User signed in');
            } else {
                return $this->sendError(401, 'Error login');
            }
        } catch (\Throwable $th) {
            return $this->sendError(401, 'User login failed');
        }
    }

    public function signup(RegisterRequest $request, UserServices $userServices)
    {
        try {
            $validated = $request->all();
            $validated['password'] = bcrypt($validated['password']);
            $user = $userServices->createUser($validated);
            $results =  $this->createNewToken($user->createToken('auth_token', ['user'])->plainTextToken);
            return $this->sendResponse($results, 'User created successfully.', 201);
        } catch (\Throwable $e) {
            return $this->sendError(401, 'User register failed');
        }
    }

    protected function createNewToken($token){
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => '',
            'user' => auth('sanctum')->user()
        ];
    }
}
