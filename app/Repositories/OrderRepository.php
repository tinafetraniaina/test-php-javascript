<?php

namespace App\Repositories;

use App\Models\Order;
use App\Models\OrderPayement;
use App\Interfaces\OrderRepositoryInterface;
use DB;

class OrderRepository implements OrderRepositoryInterface
{
    public function add($data)
    {
        return Order::create($data);
    }

    public function update($idOrder, $data)
    {
        $order = DB::table('order')
              ->where('id', $idOrder)
              ->update($data);

        return $order;
    }

    public function addItems($idOrder, $data)
    {
        foreach ($data as $key => $item) {
            $data[$key]['id_order'] = $idOrder;
        }

        return DB::table('order_product')->insert($data);
    }
}
