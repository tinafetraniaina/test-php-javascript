<?php

namespace App\Repositories;

use App\Interfaces\UserRepositoryInterface;
use App\Models\User;

class UserRepository implements UserRepositoryInterface
{
    /**
    * @param array $data
    * @return mixed
    */
    public function create($data) {
        try {
            $user = User::create($data);
            return $user;
        } catch (\Throwable $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
