<?php

namespace App\Interfaces;

interface OrderRepositoryInterface {
    public function add($data);

    public function update($idOrder, $data);

    public function addItems($idOrder, $data);

}
